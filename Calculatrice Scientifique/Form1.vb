﻿Public Class Form1
    Dim firstnum, secondnum, answer As Double
    Dim opera As String
    Dim n As Int64
    Private Sub button_click(sender As Object, e As EventArgs) Handles Button9.Click, Button8.Click, Button7.Click, Button6.Click, Button5.Click, Button4.Click, Button3.Click, Button2.Click, Button10.Click, Button1.Click, Button11.Click
        Dim b As Button = sender
        If Label1.Text = "0" Then
            Label1.Text = b.Text
        Else
            Label1.Text = Label1.Text + b.Text
        End If
    End Sub

    Private Sub Button35_Click(sender As Object, e As EventArgs) Handles Button35.Click
        If Label1.Text.Length > 0 Then
            Label1.Text = Label1.Text.Remove(Label1.Text.Length - 1, 1)
        End If
    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click
        Label1.Text = "0"
        Label2.Text = ""
    End Sub

    Private Sub Button36_Click(sender As Object, e As EventArgs) Handles Button36.Click
        If (Label1.Text.Contains("-")) Then
            Label1.Text = Label1.Text.Remove(0, 1)
        End If
    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click
        Label1.Text = "3.14159"
    End Sub

    Private Sub Button30_Click(sender As Object, e As EventArgs) Handles Button30.Click
        Dim ilog As Double
        ilog = Double.Parse(Label1.Text)
        Label2.Text = System.Convert.ToString("log" + "(" + (Label1.Text) + ")")
        ilog = Math.Log10(ilog)
        Label1.Text = System.Convert.ToString(ilog)
    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        Dim isqrt As Double
        isqrt = Double.Parse(Label1.Text)
        Label2.Text = System.Convert.ToString("√" + "(" + (Label1.Text) + ")")
        isqrt = Math.Sqrt(isqrt)
        Label1.Text = System.Convert.ToString(isqrt)
    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        Dim isinh As Double
        isinh = Double.Parse(Label1.Text)
        Label2.Text = System.Convert.ToString("sinh" + "(" + (Label1.Text) + ")")
        isinh = Math.Sinh(isinh)
        Label1.Text = System.Convert.ToString(isinh)
    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        Dim icosh As Double
        icosh = Double.Parse(Label1.Text)
        Label2.Text = System.Convert.ToString("sinh" + "(" + (Label1.Text) + ")")
        icosh = Math.Cosh(icosh)
        Label1.Text = System.Convert.ToString(icosh)
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click
        Dim itanh As Double
        itanh = Double.Parse(Label1.Text)
        Label2.Text = System.Convert.ToString("tanh" + "(" + (Label1.Text) + ")")
        itanh = Math.Tanh(itanh)
        Label1.Text = System.Convert.ToString(itanh)
    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click
        Dim isin As Double
        isin = Double.Parse(Label1.Text)
        Label2.Text = System.Convert.ToString("sin" + "(" + (Label1.Text) + ")")
        isin = Math.Sin(isin)
        Label1.Text = System.Convert.ToString(isin)
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        Dim icos As Double
        icos = Double.Parse(Label1.Text)
        Label2.Text = System.Convert.ToString("cos" + "(" + (Label1.Text) + ")")
        icos = Math.Cos(icos)
        Label1.Text = System.Convert.ToString(icos)
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        Dim itan As Double
        itan = Double.Parse(Label1.Text)
        Label2.Text = System.Convert.ToString("tan" + "(" + (Label1.Text) + ")")
        itan = Math.Tan(itan)
        Label1.Text = System.Convert.ToString(itan)
    End Sub

    Private Sub Button37_Click(sender As Object, e As EventArgs) Handles Button37.Click
        Dim a As Integer = Integer.Parse(Label1.Text)
        Label1.Text = System.Convert.ToString(a, 16)
    End Sub

    Private Sub Button34_Click(sender As Object, e As EventArgs) Handles Button34.Click
        Dim a As Integer = Integer.Parse(Label1.Text)
        Label1.Text = System.Convert.ToString(a, 2)
    End Sub

    Private Sub operation_arithmetique(sender As Object, e As EventArgs) Handles Button29.Click, Button27.Click, Button15.Click, Button14.Click, Button13.Click, Button12.Click
        Dim ops As Button = sender
        firstnum = Label1.Text
        Label2.Text = Label1.Text
        Label1.Text = ""
        opera = ops.Text
        Label2.Text = Label2.Text + "" + opera
    End Sub

    Private Sub Button26_Click(sender As Object, e As EventArgs) Handles Button26.Click
        Dim a As Double
        a = Convert.ToDouble(Label1.Text) * Convert.ToDouble(Label1.Text)
        Label1.Text = System.Convert.ToString(a)
    End Sub

    Private Sub Button28_Click(sender As Object, e As EventArgs) Handles Button28.Click
        Dim a As Double
        a = Convert.ToDouble(Label1.Text) * Convert.ToDouble(Label1.Text) * Convert.ToDouble(Label1.Text)
        Label1.Text = System.Convert.ToString(a)
    End Sub

    Private Sub Button33_Click(sender As Object, e As EventArgs) Handles Button33.Click
        Dim a As Double
        a = Convert.ToDouble(1.0 / Convert.ToDouble(Label1.Text))
        Label1.Text = System.Convert.ToString(a)
    End Sub

    Private Sub Button31_Click(sender As Object, e As EventArgs) Handles Button31.Click
        Dim iln As Double
        iln = Double.Parse(Label1.Text)
        Label2.Text = System.Convert.ToString("lin" + "(" + (Label1.Text) + ")")
        iln = Math.Log(iln)
        Label1.Text = System.Convert.ToString(iln)
    End Sub

    Private Sub Button32_Click(sender As Object, e As EventArgs) Handles Button32.Click
        Dim a As Double
        a = Convert.ToDouble(Label1.Text) / Convert.ToDouble(100)
        Label1.Text = System.Convert.ToString(a)
    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click
        secondnum = Label1.Text
        If opera = "+" Then
            answer = firstnum + secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "-" Then
            answer = firstnum - secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "*" Then
            answer = firstnum * secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "/" Then
            answer = firstnum / secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "mod" Then
            answer = firstnum Mod secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "Exp" Then
            answer = firstnum ^ secondnum
            Label1.Text = answer
            Label2.Text = ""
        End If
    End Sub

    Private Sub Label1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Label1.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
End Class
